postgresql-plproxy (2.11.0-12) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Restrict to 64-bit architectures.

 -- Christoph Berg <myon@debian.org>  Sat, 14 Sep 2024 23:42:04 +0200

postgresql-plproxy (2.11.0-11) unstable; urgency=medium

  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.

 -- Christoph Berg <myon@debian.org>  Mon, 18 Sep 2023 21:47:19 +0200

postgresql-plproxy (2.11.0-10) unstable; urgency=medium

  * New upstream version.
  * Drop build-time tests, don't work without access to /var/run/postgresql.
  * Update homepage.

 -- Christoph Berg <myon@debian.org>  Fri, 15 Sep 2023 16:27:10 +0200

postgresql-plproxy (2.10.0-9) unstable; urgency=medium

  * Remove newpid from test dependencies.

 -- Christoph Berg <myon@debian.org>  Mon, 04 Sep 2023 10:26:19 +0000

postgresql-plproxy (2.10.0-8) unstable; urgency=medium

  * Upload for PostgreSQL 15.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Oct 2022 16:12:01 +0200

postgresql-plproxy (2.10.0-7) unstable; urgency=medium

  * Upload for PostgreSQL 14.

 -- Christoph Berg <myon@debian.org>  Thu, 21 Oct 2021 10:21:41 +0200

postgresql-plproxy (2.10.0-6) unstable; urgency=medium

  * Remove tests that show varying output with PG14 libpq5, there is
    additional variation in 127.0.0.1 vs. ::1.

 -- Christoph Berg <myon@debian.org>  Fri, 01 Oct 2021 15:19:45 +0200

postgresql-plproxy (2.10.0-5) unstable; urgency=medium

  * Add plproxy_test_4.out file for PG<=10 with PG14 libpq5 output.

 -- Christoph Berg <myon@debian.org>  Mon, 13 Sep 2021 11:19:25 +0200

postgresql-plproxy (2.10.0-4) experimental; urgency=medium

  * Support PostgreSQL 14.

 -- Christoph Berg <myon@debian.org>  Thu, 24 Jun 2021 14:40:39 +0200

postgresql-plproxy (2.10.0-3) unstable; urgency=medium

  * Skip build-time tests with newnet/newpid on non-Linux.

 -- Christoph Berg <myon@debian.org>  Wed, 04 Nov 2020 15:20:08 +0100

postgresql-plproxy (2.10.0-2) unstable; urgency=medium

  * Add test-dependency on postgresql-common (>= 217~). (Closes: #972045)

 -- Christoph Berg <myon@debian.org>  Mon, 12 Oct 2020 23:14:45 +0200

postgresql-plproxy (2.10.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove unnecessary XS-Testsuite field in debian/control.

  [ Christoph Berg ]
  * New upstream version.
  * Upload for PostgreSQL 13.
  * Use dh --with pgxs and run tests at build-time as well.
  * DH 13.
  * R³: no.
  * debian/tests: Simplify.

 -- Christoph Berg <myon@debian.org>  Fri, 09 Oct 2020 11:37:46 +0200

postgresql-plproxy (2.9-2) unstable; urgency=medium

  * Upload for PostgreSQL 12.

 -- Christoph Berg <myon@debian.org>  Wed, 30 Oct 2019 16:57:26 +0100

postgresql-plproxy (2.9-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Mon, 16 Sep 2019 15:08:59 +0200

postgresql-plproxy (2.8-3) unstable; urgency=medium

  * tests: Recent PostgreSQL minors (11.2 and friends) disallow setting
    client_min_messages higher than 'error', so suppressing errors that way
    does not work anymore. Instead, drop the "create language plpgsql" code as
    plpgsql is preinstalled by default since 9.1.
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 14 Feb 2019 16:54:09 +0100

postgresql-plproxy (2.8-2) unstable; urgency=medium

  * Upload for PostgreSQL 11.
  * Move packaging repository to salsa.debian.org
  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <christoph.berg@credativ.de>  Fri, 19 Oct 2018 10:18:52 +0200

postgresql-plproxy (2.8-1) unstable; urgency=medium

  * New upstream release.
  * Run tests via newnet/localhost so they don't conflict with an existing
    cluster on 5432. (Closes: #856423)

 -- Christoph Berg <myon@debian.org>  Sat, 14 Oct 2017 16:21:59 +0200

postgresql-plproxy (2.7-2) unstable; urgency=medium

  * Team upload.
  * Add patch to support PG 10 in Makefile.
    https://github.com/plproxy/plproxy/pull/27

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 21 Sep 2017 14:58:44 +0200

postgresql-plproxy (2.7-1) unstable; urgency=medium

  * New upstream release.

 -- Christoph Berg <myon@debian.org>  Thu, 29 Dec 2016 18:28:39 +0100

postgresql-plproxy (2.6-3) unstable; urgency=medium

  * Pull patches from upstream for 9.6 support.

 -- Christoph Berg <myon@debian.org>  Sun, 25 Sep 2016 23:03:59 +0200

postgresql-plproxy (2.6-2) unstable; urgency=medium

  * Reupload with 9.5 support.
  * Run tests on port 5432 using newpid/newnet.

 -- Christoph Berg <myon@debian.org>  Sat, 09 Jan 2016 18:54:22 +0100

postgresql-plproxy (2.6-1) unstable; urgency=medium

  * New upstream release.
    + Docs are now markdown (Closes: #795984, thanks to Eduard Sanou for the
      original patch which is now obsolete)
  * Drop all patches :)
  * Update watch file to point at plproxy.github.io.
  * Update rules to dh.

 -- Christoph Berg <myon@debian.org>  Sat, 05 Sep 2015 21:03:05 +0200

postgresql-plproxy (2.5-5) unstable; urgency=medium

  * Rebuild against PostgreSQL 9.4.
  * Compile sql/plproxy.sql for the non-extension builds.
  * Set team as maintainer.

 -- Christoph Berg <myon@debian.org>  Sun, 27 Jul 2014 11:44:14 +0200

postgresql-plproxy (2.5-4) unstable; urgency=low

  [ Martin Pitt ]
  * debian/tests/installcheck: Temporarily modify test scripts to use CREATE
    EXTENSION instead of sourcing the build tree's plproxy.sql. We want to
    test the installed package and avoid building.

  [ Christoph Berg ]
  * Use CREATE EXTENSION for 9.1+ only.

 -- Christoph Berg <myon@debian.org>  Sat, 08 Feb 2014 12:14:28 +0100

postgresql-plproxy (2.5-3) unstable; urgency=low

  [ Martin Pitt ]
  * Update Vcs-* tags to current git.
  * Add postgresql-server-dev-all test dependency as the test uses
    pg_buildext.
  * debian/tests/installcheck: Create build-$version if it does not exist.

  [ Christoph Berg ]
  * Call rm -rf build-*/ in clean instead of using pg_buildext (doesn't work
    because build-*/doc is missing).

 -- Christoph Berg <myon@debian.org>  Fri, 01 Nov 2013 22:38:13 +0100

postgresql-plproxy (2.5-2) unstable; urgency=low

  * Add autopkgtest support.
  * Use "all" in debian/pgversions.
  * Makefile: Remove the old vpath patch (now better supported by PostgreSQL),
    and remove the recursion into doc which doesn't work in build-%v/.
  * src/plproxy.h: include libpq-fe.h earlier to un-mess pg_int64 when using
    libpq-dev from 9.3.

 -- Christoph Berg <myon@debian.org>  Fri, 13 Sep 2013 13:51:07 +0200

postgresql-plproxy (2.5-1) experimental; urgency=low

  * New upstream release.

 -- Christoph Berg <myon@debian.org>  Thu, 20 Dec 2012 15:50:35 +0100

postgresql-plproxy (2.4-1) unstable; urgency=low

  * New upstream release.
  * "build" target needs DESTDIR defined too, temporarily put a version of
    pg_buildext in debian/ that does this.

 -- Christoph Berg <myon@debian.org>  Wed, 13 Jun 2012 16:14:51 +0200

postgresql-plproxy (2.3-1) unstable; urgency=low

  [ Christoph Berg ]
  * Add 8.2 and 8.3 to supported versions.

  [ Peter Eisentraut ]
  * New upstream release
    - Obsoletes patches/doc

 -- Peter Eisentraut <petere@debian.org>  Wed, 02 Nov 2011 21:10:16 +0200

postgresql-plproxy (2.2-3) unstable; urgency=low

  * Rebuild for PostgreSQL 9.1.
  * Remove our private pg_buildext copy.

 -- Christoph Berg <myon@debian.org>  Fri, 17 Jun 2011 17:13:08 +0200

postgresql-plproxy (2.2-2) unstable; urgency=low

  * Building the documentation needs asciidoc.

 -- Christoph Berg <myon@debian.org>  Tue, 05 Apr 2011 18:05:27 +0200

postgresql-plproxy (2.2-1) unstable; urgency=low

  * New upstream release.
  * Fix Vcs-* fields.
  * Support building for multiple PostgreSQL versions (8.4 and 9.0 now).
  * Install documentation.
  * Add myself as Maintainer.

 -- Christoph Berg <myon@debian.org>  Fri, 01 Apr 2011 14:51:55 +0200

postgresql-plproxy (2.1-1) unstable; urgency=low

  * New upstream release.
  * Update policy version 3.9.1.
  * Changed source format to 3.0 (quilt)
  * Added provisions for building with mismatching libpq-dev and
    postgresql-server-dev installed.
  * Changed section to database
  * Fixed copyright file to match reality

 -- Peter Eisentraut <petere@debian.org>  Thu, 05 Aug 2010 21:49:06 +0300

postgresql-plproxy (2.0.8-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control
    - support PostrgeSQL 8.4, changing build-depends, depends and binary package
      name; Closes: #536973, #559596
  * debian/install
    - install into 8.4 directory

 -- Sandro Tosi <morph@debian.org>  Thu, 11 Feb 2010 13:51:21 +0100

postgresql-plproxy (2.0.8-1) unstable; urgency=low

  * New Upstream Release
  * Added README.source

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Thu, 05 Mar 2009 16:54:09 -0300

postgresql-plproxy (2.0.7-1) unstable; urgency=low

  * New upstream release.

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Fri, 09 Jan 2009 15:51:48 -0200

postgresql-plproxy (2.0.5-2) unstable; urgency=low

  * Fix directory /usr/share/postgresql-8.2-plproxy to 8.3.
  * Update to debhelper 7.

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Tue, 08 Jul 2008 11:22:55 -0300

postgresql-plproxy (2.0.5-1) unstable; urgency=low

  * New upstream release
  * Update policy version to 3.8.0.
  * Added Vcs-* control fields.
  * Added Homepage control field.

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Mon, 09 Jun 2008 13:47:37 -0300

postgresql-plproxy (2.0.4-1) unstable; urgency=low

  * New upstream release.

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Tue, 05 Feb 2008 12:09:02 -0200

postgresql-plproxy (2.0.2-1) unstable; urgency=low

  * Initial release. (Closes: #427293)

 -- Fernando Ike de Oliveira <fike@midstorm.org>  Tue, 05 Jun 2007 23:19:56 -0300
